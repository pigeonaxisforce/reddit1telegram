""" Distributed under GNU Affero GPL.

    Reddit1Telegram v0.5 "Andreyev" 30/01/2021.

    Copyright (C) 2020  Davide Leone

    You can contact me at leonedavide[at]protonmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

import logging
import json
import yaml
import pickle
import requests
import time
from logging.handlers import RotatingFileHandler
from pathlib import Path
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Filters, MessageHandler, Updater

with open('config.yaml') as file:
    config = yaml.load(file, Loader=yaml.FullLoader)

allowed_post_types = config['allowed_post_types'] # Type: dictionary
always_on = config['always_on']  # Type: boolean
channel_id = config['channel_id']  # Type: str or int. Channel username is a valid entry
database_path = config['default_path'] + config['database_file']  # Type: str
format_link_post = config['format_link_post']  # Type: str
format_image_post = config['format_image_post']  # Type: str
format_textual_post = config['format_textual_post']  # Type: str
format_video_post = config['format_video_post']  # Type: str
headers = config['headers']  # Type: dictionary
hours = config['hours']  # Type: list
logging_level = config['logging_level']  # Type: str DEBUG, INFO, WARNING, ERROR and CRITICAL are the only valid entries
logging_path = config['default_path'] + config['logging_file']  # Type: str
logging_size = config['logging_file_size']
private_chat_message = config['private_chat_message']  # Type: str
private_chat_message_formatting = config[
    'private_chat_message_formatting']  # Type: str MarkdownV2, Markdown, HTML and None are the only valid entries
posts_number = config['posts_number']  # Type: int
refresh_rate = config['refresh_rate']  # Type: int
quoting_keyboard_text = config['quoting_keyboard_text']  # Type: str
quoting_keyboard_link_post = config['quoting_keyboard_link_post']  # Type: boolean
quoting_keyboard_image_post = config['quoting_keyboard_image_post']  # Type: boolean
quoting_keyboard_textual_post = config['quoting_keyboard_textual_post']  # Type: boolean
quoting_keyboard_video_post = config['quoting_keyboard_video_post']  # Type: boolean
source = config['source']  # Type: str
subreddits = config['subreddits']  # Type: list, each entry being a link ending in .json to a subreddit
token = config['token']  # Type: str

if posts_number == 0:  # If posts number is set to be zero, the bot will send as many posts as it can
    posts_number = 25  # Currently a .json feed by Reddit contains exactly 25 posts

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    handlers=[
                        logging.StreamHandler(),  # This header prints logs
                        RotatingFileHandler(logging_path,
                                            maxBytes=logging_size,
                                            backupCount=1)
                    ],  # This header saves logs to file
                    level=logging_level)

logging.info('Reddit1Telegram Andreyev version was started. Configuration loaded')

updater = Updater(token=token)  # This function is used to connect to Telegram

if Path(database_path).is_file():
    # Check if the database file exists.
    # If it does exist the database is loaded. Otherwise the file is created.
    # database type: list

    logging.info('Database found')
    with open(database_path, 'rb') as file:
        database = pickle.load(file)

else:
    logging.warning('Database not found')
    with open(database_path, 'wb') as file:
        pickle.dump([], file)
    database = []


def callback(update, context):
    """
    This function is used when a message is sent to the bot from a private chat and the bot is configured to be always
    on. A reply is sent to the user, containing a predefined text message and an inline keyboard with the source code
    of the bot itself.
    Please notice that not providing the URL of the source code might be a violation of the Affero license.
    """

    logging.debug('callback function called')

    update.message.reply_text(
        text=private_chat_message,
        parse_mode=private_chat_message_formatting,
        reply_markup=InlineKeyboardMarkup(
            [[
                InlineKeyboardButton(
                    text='Source code',
                    url=source
                )
            ]]
        )
    )

    time.sleep(1)  # A simple anti flood protection


def get_feed(url: str, headers=headers) -> dict:
    """
    :param url: URL of a subreddit ending in .json e.g. https://www.reddit.com/r/Telegram/hot.json
    :param headers: headers for requests function
    :return: Data on 25 different posts
    :rtype: dictionary
    A page of this kind contains data on subreddit's posts, sorted by new, hot or trending (in the example, hot).
    We are only interested in information contained in ['data']['children'].
    """

    logging.debug('get_feed function called with url ' + url)

    feed = requests.get(url, headers=headers)
    posts = json.loads(feed.text)['data']['children']
    return posts


def update_database() -> bool:
    """
    When the database variable is updated, this function is used to update the related file.
    """

    logging.debug('update_database function called')

    with open(database_path, 'wb') as file:
        pickle.dump(database, file)

    return True


def send_long_text(message: str, keyboard=None, max_length=4096, chat_id=channel_id) -> bool:
    """
    This function is used to divide a long text message into smaller one
    :param chat_id: Target chat
    :param message: A long text impossible to send with a single sendMessage
    :param keyboard: Inline keyboard
    :param max_length: Max length of each message to send
    A message can contain at most 4096 characters. Source: https://core.telegram.org/bots/api#message
    he message is split into words, that are later recomposed to form a text as long as possible
    The text is then sent, and remaining words are used to compose another message until no more words are left
    Please notice that this function does not supports wall of text (long text with no spaces).
    """

    logging.debug('send_long_text function called with a ' + str(len(message)) + " characters long text")
    words = message.split()  # Text is split into words
    text = ''

    for z in range(len(words)):
        if len(text + words[z]) <= max_length:
            # If adding a word won't cause the text to exceed the limit, a word is added
            text += ' ' + words[z]
            if z + 1 == len(words):
                # We have added the last remaining word to the text
                # Therefore, the last message is sent and the function returns True
                # Please notice that the inline keyboard is sent only with the last message

                logging.debug('The last message is ready to be sent. It has ' + str(len(text)) + ' characters')
                updater.bot.sendMessage(chat_id=chat_id,
                                        text=text,
                                        parse_mode=None,
                                        reply_markup=keyboard)
                return True
            else:
                # We can continue to add words to the text
                pass
        else:
            # Adding a word would cause the text to exceed the limit. Therefore, no more word can be added
            # The text is sent. Another message will now be composed

            logging.debug('A message is ready to be sent. It has ' + str(len(text)) + ' characters. ' +
                          str(len(words)) + ' words left to be sent')
            updater.bot.sendMessage(chat_id=chat_id,
                                    text=text,
                                    parse_mode=None)
            text = ''


def send_post(posts: list, chat_id=channel_id) -> bool:
    """
    This yield function is used to send a single post to the specified channel
    :param posts: A dictionary each entry of which contains data on a specific post
    :param chat_id: Target chat
    Every entry is evaluated till the bot finds a post that was never sent to the channel and can successfully send it
    As soon as a post is sent to the channel, the function stops returning True
    As the function stops as soon as a valid post (i.e. not already sent) is found, we can assume every post evaluated
    is unsuitable. Thus we do not need to check these posts again, and for this reason the function is a yield
    """
    global database

    logging.debug('send_post function called with ' + str(len(posts)) + ' posts submitted')

    for y in range(len(posts)):
        logging.debug('Post ' + posts[y]['data']['url'] + ' will now be processed')

        post_id = posts[y]['data']['name']
        # post_id is a str value that uniquely identifies a post in a subreddit e.g. t3_hgp2dp
        # By storing this value in the database we can know which posts were sent and avoid re-sending them

        post_url = 'redd.it/'+ post_id.split('_')[1]
        # Is worth noticing that a post with ID t3_hgp2dp has redd.it/hgp2dp as a valid permalink

        if post_id in database:
            # This post was already sent. Therefore, the bot move on to the next post.
            logging.debug('A post was found in database. The bot move on to the next post')
            continue
            
        if 'crosspost_parent_list' in posts[y]['data'].keys(): # Cross-post post (post from another subreddit)
            post_data = posts[y]['data']['crosspost_parent_list'][0]
            logging.debug("A cross-post is ready to be processed")

        else:
            post_data = posts[y]['data']

        try:
            if post_data['selftext'] != '':  # Post is a text message
                logging.debug('A text message is ready to be sent')

                if allowed_post_types['text']:
                    text = format_textual_post.format(author='u/' + post_data['author'],
                                                      upvotes=post_data['ups'],
                                                      subreddit='r/' + post_data['subreddit'],
                                                      text=post_data['selftext'],
                                                      title=post_data['title'],
                                                      permalink=post_url)

                    if quoting_keyboard_textual_post:
                        # If configured to be True, textual post will show an inline keyboard a link to the original post
                        keyboard = InlineKeyboardMarkup([[InlineKeyboardButton(text=quoting_keyboard_text, url=post_url)]])
                    else:
                        keyboard = None

                    if len(text) > 4096:
                        # Texts longer than 4096 characters cannot be sent by Telegram, therefore are split in smaller ones
                        send_long_text(text, keyboard=keyboard)
                    else:
                        updater.bot.sendMessage(chat_id=chat_id,
                                                text=text,
                                                parse_mode=None,
                                                reply_markup=keyboard)

            elif post_data['domain'] == 'v.redd.it':  # Post is a video or a GIF
                # Please notice that video are currently sent WITHOUT AUDIO
                logging.debug('A video is ready to be sent')

                if allowed_post_types['video']:

                    if quoting_keyboard_video_post:
                        keyboard = InlineKeyboardMarkup([[InlineKeyboardButton(text=quoting_keyboard_text, url=post_url)]])
                    else:
                        keyboard = None

                    caption = format_video_post.format(author='u/' + post_data['author'],
                                                       upvotes=post_data['ups'],
                                                       url=post_data['url'],
                                                       subreddit='r/' + post_data['subreddit'],
                                                       title=post_data['title'],
                                                       permalink=post_url)

                    updater.bot.sendVideo(chat_id=chat_id,
                                          video=post_data['media']['reddit_video']['fallback_url'],
                                          caption=caption,
                                          reply_markup=keyboard)

            elif post_data['domain'] == 'i.redd.it' and post_data['url'][-4:] == '.gif':
                # Post is a GIF
                logging.debug('A GIF is ready to be sent')

                if allowed_post_types['video']:

                    if quoting_keyboard_video_post:
                        keyboard = InlineKeyboardMarkup([[InlineKeyboardButton(text=quoting_keyboard_text, url=post_url)]])
                    else:
                        keyboard = None

                    caption = format_video_post.format(author='u/' + post_data['author'],
                                                       upvotes=post_data['ups'],
                                                       url=post_data['url'],
                                                       subreddit='r/' + post_data['subreddit'],
                                                       title=post_data['title'],
                                                       permalink=post_url)

                    updater.bot.sendDocument(chat_id=chat_id,
                                             document=post_data['url']+'?format=mp4',
                                             # We noticed that reddit links ending in .gif are not accepted by Telegram
                                             #   But reddit links ending in .gif?format=mp4 are
                                             caption=caption,
                                             reply_markup=keyboard)


            elif post_data['domain'] == 'i.redd.it' or post_data['url'][-4:] == '.png' \
                    or  post_data['url'][-4:] == '.jpg':  # Post is an image

                logging.debug('An image is ready to be sent')

                if allowed_post_types['images']:

                    if quoting_keyboard_image_post:
                        keyboard = InlineKeyboardMarkup([[InlineKeyboardButton(text=quoting_keyboard_text, url=post_url)]])
                    else:
                        keyboard = None

                    caption = format_image_post.format(author='u/' + post_data['author'],
                                                       upvotes=post_data['ups'],
                                                       subreddit='r/' + post_data['subreddit'],
                                                       title=post_data['title'],
                                                       url=post_data['url'],
                                                       permalink=post_url)

                    updater.bot.sendPhoto(chat_id=chat_id,
                                          photo=post_data['url'],
                                          caption=caption,
                                          reply_markup=keyboard)

            else:  # Post is a link
                logging.debug('A link post is ready to be sent')


                if allowed_post_types['link']:

                    if quoting_keyboard_link_post:
                        keyboard = InlineKeyboardMarkup([[InlineKeyboardButton(text=quoting_keyboard_text, url=post_url)]])

                    else:
                        keyboard = None

                    text = format_textual_post.format(author='u/' + post_data['author'],
                                                      url=post_data['url'],
                                                      upvotes=post_data['ups'],
                                                      subreddit='r/' + post_data['subreddit'],
                                                      text=post_data['selftext'],
                                                      title=post_data['title'],
                                                      permalink=post_url)

                    updater.bot.sendMessage(chat_id=chat_id,
                                            text=text,
                                            reply_markup=keyboard)

            # A post was successfully processed. Therefore, its ID is added to the database and the database updated
            logging.info('A post was successfully processed. Its ID will now be stored')
            database.append(post_id)
            update_database()

            yield True

        except Exception as e:
            logging.error('An error occurred while trying to send a post')
            logging.error(e)

def main() -> None:
    """
    This function is used to retrieve posts from subreddits, and then sent new posts (i.e. posts that the bot never
    sent before) to the channel.
    """
    logging.info('main function called')

    for subreddit in subreddits:
        try:
            posts = get_feed(subreddit)
        except Exception as e:
            logging.critical('An error occurred while getting the feed for subreddit ' + subreddit)
            logging.critical(e)
            posts = []

        logging.debug(str(posts_number) + " posts from " + subreddit + " will now be sent to the channel")

        send = send_post(posts)  # Send post is a yield function

        for x in range(posts_number):
            try:
                # A post is sent to the channel
                send.__next__()
                time.sleep(3)  # A simple anti flood protection (Telegram allows 20 posts in 60 seconds)
            except StopIteration:
                # This is the case in which there are no more posts suitable to be published in the given subreddit
                # Therefore, the cycle stops
                logging.debug('Stop iteration: no more suitable posts in ' + subreddit)
                break

    logging.info('main function ended')


if __name__ == '__main__':

    if always_on:
        # When always_on is set to be True, the bot is supposed to continue running
        # Thus, a handler is added to answer to every message in private chat
        # If "hours" is not set in the config file, the bot will search for new content every 5 minute and publish
        #  them as soon as possible. This setting is particular recommended if you want to continuously receive
        #  posts from the "NEW" feed of a subreddit.
        # If "hours" setting was configured, the bot will search for new content and publish then
        #  only at given hours, once per hours. For example, if hours is set to [10,15] the bot will publish new posts,
        #  if possible, at 10 AM and 3 PM.

        updater.dispatcher.add_handler(MessageHandler(filters=Filters.chat_type.private, callback=callback))
        updater.start_polling()  # The bot will receive messages sent to the bot on Telegram

        if not hours:
            while True:
                main()
                time.sleep(refresh_rate)
        else:  # The bot will posts only in specified hours
            while True:
                if time.localtime().tm_hour in hours:
                    main()
                    time.sleep(refresh_rate)
                else:
                    time.sleep(50)

    else:
        # When always_on is set to be False, the bot is supposed to publish new content and then end the process
        main()

    logging.info('Instructions end')
