# Reddit1Telegram

Reddit1Telegram is a Python software that will allow you to receive subreddits' feeds to a channel, group or to your private chat.

You can find us on Telegram at [t.me/reddit1telegram](https://t.me/reddit1telegram) and at [t.me/teaproject](https://t.me/teaproject)

# Features

- Can be used in channels, groups and private chats; 
- Can be used with **multiple subreddits**; 
- Works with different feeds (new, hot, top); 
- Works with **every kind of post**; 
- **Automatically posts at specified hours and with specified frequency** (e.g. every 5 minutes); 
- Easily **configurable formattation** for posts with optional inline keyboard to original post; 
- Link to the original post is shortened. 

## Planned features 

- Audio support for video;
- Markdown support.

# Authors

Reddit1Telegram was created (and is currently maintained) by @davideleone and is officially supported by the @teaproject 

A big thanks to our wonderful contributors: 
- @octospacc
- @onionmaster03

# How to use Reddit1Telegram

### Step 1: download it

Download the source code using the following command: 

`$ git clone https://gitlab.com/teaproject/reddit1telegram`

If you are not sure if you do comply with Reddit1Telegram requirements please run the following command:

`$ pip3 install -r requirements.txt`

### Step 2: edit the config file

Comments in **config.yaml** explain each setting and how editing them impacts bot's activity. Feel free to edit the config file according to your needs. 
Please contact [@BotFather](https://t.me/botfather) to obtain your bot's token, and add it to the config file. 

### Step 3: run!

Simply run the main.py file! You can run it with the following command:

`$ python3 main.py`

### Specific scenarios

#### Multiple channels

If you want to create another channel - send a different feed to a different chat - just create a new folder, and start again from step 1. 

#### Absolute path 

In some cases you might need to specify the absolute path to the config.yaml file - e.g. using Linux cronjob. To do so just modify _"config.yaml"_ in main.py file, first line after imports, with file's absolute path.

# Versioning conventions

We follow the simple _Major.Minor.Patch_ convention.

Each version release will be named after a Russian writer, alphabetically. Therefore, 0.x versions are named after [Andreyev](https://en.wikipedia.org/wiki/Leonid_Andreyev). 
1.x versions will be named after [Bulgakov](https://en.wikipedia.org/wiki/Mikhail_Bulgakov).
